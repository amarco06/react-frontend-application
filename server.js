const express = require('express');
const cookieParser = require('cookie-parser')
const mongoose = require('mongoose');
const cors = require('cors');
const multer = require('multer');
const validator = require('express-validator')

const session = require('express-session')
const path = require('path');
const MongoStore = require('connect-mongo');
const ENV = 'http://localhost:4000'
const MONGO_DB_URL = 'mongodb+srv://admin:admin@b106.2xr0i.mongodb.net/batch106_ecommerce?retryWrites=true&w=majority'
const productRoutes = require('./routes/productRoutes');
const userRoutes = require('./routes/userRoutes');
const cartRoutes = require('./routes/cartRoutes');

mongoose.connect(MONGO_DB_URL, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useFindAndModify: false,
  useCreateIndex: true,
})

mongoose.connection.once('open', () => console.log('Now connected to the Database.'))

const app = express();

const fileStorageEngine = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, './public/images')
  },
  filename: (req, file, cb) => {
    cb(null, Date.now() + "--" + file.originalname)
  }
})

const upload = multer({ storage: fileStorageEngine })
app.use(express.static(path.join(__dirname, 'public')));
app.use(cookieParser());
app.use(cors({credentials: true, origin: 'http://localhost:3000'}))
app.use(express.json());	//incoming Request Object as a JSON Object
app.use(express.urlencoded({ extended: true })); //Object as strings or arrays
app.use(
  session({
    secret: 'secret',
    resave: false,
    saveUninitialized: false,
    store: MongoStore.create({ mongoUrl: MONGO_DB_URL }),
    cookie: { path: '/', maxAge: 180 * 60 * 1000, secure: false, httpOnly: false}
  })
);

app.use(function(req, res , next) {
  res.locals.session = req.session
  next();
})

app.post("/single", upload.single("images"), (req, res) => {
  res.send({
    msg: 'upload success',
    data: `${ENV}/images/${req.file.filename}`
  });
})

app.use("/products", productRoutes);
app.use("/users", userRoutes);
app.use("/cart", cartRoutes);

app.listen(process.env.PORT || 4000, () => {
  console.log(`API is now online on port ${process.env.PORT || 4000}`)
});