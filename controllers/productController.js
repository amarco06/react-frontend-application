const Product = require('../models/product');
const bcrypt = require('bcrypt');
const auth = require('../auth');
const { validationResult } = require("express-validator")

//Create product (Admin only)
module.exports.createProduct = async (req, res) => {
	try {
		if (!auth.decode(req.headers.authorization).isAdmin){
			return res.status(400).send('Unauthorized access.');
		};
		const errors = validationResult(req); // Finds the validation errors in this request and wraps them in an object with handy functions
		if (!errors.isEmpty()) {
			res.status(400).json({ errors: errors.array() });
			return;
		};
		let params = req.body
		let product = new Product({
			name       : params.name,
			description: params.description,
			quantity   : params.quantity,
			price      : params.price,
			image      : params.image,
			isActive   : true
		})
		await product.save();
		res.status(200).send(product);
	} catch (error) {
		res.status(500).send("Something went wrong");
	}
};

//Retrieve all active products
module.exports.getActiveProducts = async (req, res) => {
	try {
		res.status(200).send(await Product.find({ isActive:true }));
		
	} catch(error) {
		res.status(500).send("Something went wrong");
	}
};

//Retrieve single product
module.exports.getProductsById = async (req, res) => {
	try {
		let productId = req.params.id
		res.status(200).send(await Product.findById(productId));

	} catch(error) {
		res.status(500).send("Something went wrong");
	}
};

//Update product
module.exports.updateProduct = async (req, res) => {
	try {
		if (!auth.decode(req.headers.authorization).isAdmin){
			return res.status(400).send('Unauthorized access.');
		};

		const errors = validationResult(req); // Finds the validation errors in this request and wraps them in an object with handy functions
		if (!errors.isEmpty()) {
			res.status(400).json({ errors: errors.array() });
			return;
		}

		let newContent = req.body
		let productId = req.params.id
		let product = await Product.findById(productId)
		product.name =  newContent.name
		product.description = newContent.description
		product.price = newContent.price
		product.quantity = newContent.quantity
		product.image = newContent.image
		
		await product.save();
		res.status(200).send(product);

	} catch (error) {
		res.status(500).send("Something went wrong"); //outside validation error
	}
};

//Archive a product
module.exports.archiveProduct = (req, res) => {
	try {
		if (!auth.decode(req.headers.authorization).isAdmin){
			return res.status(400).send('Unauthorized access.');
		}

		let productId = req.params.id

		Product.findById(productId, { isActive: true }).then(product => {
			product.isActive = false;
			return product.save().then((updatedProduct, hasErr) => {
				res.status(400).send(!hasErr);
			})
		})
	} catch (error) {
		res.status(500).send("Something went wrong"); //outside validation error
	}
};


