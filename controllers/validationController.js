const User = require('../models/user');
const { body } = require("express-validator")

module.exports.validate = (method) => {
	switch (method) {
		case 'register': {
			return [
				body('firstName', 'firstName is required.')
					.exists()
					.notEmpty(),
				body('lastName', 'lastName is required.')
					.exists()
					.notEmpty(),
				body('email', 'Email is required.')
					.exists()
					.notEmpty()
					.isEmail()
					.custom(async value => {
						const existingUser = await User.findOne({ email: value })
						if (existingUser) {
							throw new Error('Email already in use.')
						}
					}),
				body('password', 'Invalid password.')
					.exists()
					.notEmpty(),
				body('contactNo')
					.optional()
					.notEmpty()
					.isInt()
			];
		};

		case 'saveProduct': {
			return [
				body('name', 'Name is required.')
					.exists()
					.notEmpty(),
				body('description', 'Description is required.')
					.exists()
					.notEmpty(),
				body('quantity', 'Quantity is required.')
					.exists()
					.notEmpty(),
				body('price', 'Price is required.')
					.exists()
			];
		};

		case 'createOrder': {
			return [
				body('productId', 'productId is required.')
					.exists()
					.notEmpty(),
				body('quantity', 'Quantity is required.')
					.exists()
					.isInt()
			];
		}
	}
};