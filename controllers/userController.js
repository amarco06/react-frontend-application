const User = require('../models/user');
const Product = require('../models/product');
const Purchase = require('../models/purchase');
const bcrypt = require('bcrypt');
const auth = require ('../auth');
const { validationResult } = require("express-validator")

exports.register = async (req, res) => {
	try {
  		const errors = validationResult(req);
  		if (!errors.isEmpty()) {
  			res.status(400).send({ errors: errors.array() });
  			return;
  		}
  		const data = req.body
  		let user = new User({
  			firstName: data.firstName,
  			lastName: data.lastName,
  			email: data.email,
  			contactNo: data.contactNo,
  			isAdmin: data.isAdmin,
  			password: data.password && bcrypt.hashSync(data.password, 10)
  		})

  		await user.save();
  		res.status(200).send(user);
  	} catch (error) {
  		res.status(500).send("Something went wrong");
  	}
  };

//User Login
module.exports.login = (req, res) => {
	try {
		User.findOne({ email: req.body.email }).then( user => {
			if (user === null){
				return res.status(400).send("Email does not exist!");
			}
			const isPasswordMatched = bcrypt.compareSync(req.body.password, user.password)
			if(isPasswordMatched){
				res.status(200).json({ accessToken: auth.createAccessToken(user.toObject()) });
			} else {
				return res.status(400).send("Incorrect password!");
			}
		})
	} catch (error){
		res.status(500).send("Something went wrong");
	}
	
};

//Get User info
module.exports.getUserInfo = async (req, res) => {
	try {
		const user = auth.decode(req.headers.authorization)
		let result = await User.findById({ _id: user.id })
		return res.status(200).send(result);
	} catch (error){
		res.status(500).send("Something went wrong");
	}
};

module.exports.getPurchaseHistory = async (req,res) => {
	try {
		const user = auth.decode(req.headers.authorization)
		let purchases = await Purchase.find({ userId: user.id })
		let store = []
		for (var key in purchases) {
			let subTotal = 0
			if (purchases.hasOwnProperty(key)) {
				let productPurchase = purchases[key].orders.map(async item => {
					subTotal += item.totalAmount
					return {
						product: await Product.findById(item.productId).select('-customers -__v'),
						quantity: item.quantity,
						totalAmount: item.totalAmount
					}
				})
				await Promise.all(productPurchase).then(data => {
					store.push({
						orders: data,
						subTotal: subTotal
					})
				})
			}
		}
		return res.status(200).send(store);
	} catch (error){
		res.status(500).send("Something went wrong");
	}
}

module.exports.removeProductInOrder = async (req, res) => {
	try {
		const user = auth.decode(req.headers.authorization)
		let result = await User.findById({ _id: user.id })
		let orders = result.orders.filter(order => order.productId !== req.params.id)
		result.orders = orders;
		result.save()
		return res.status(200).send(result);
	} catch (error){
		res.status(500).send("Something went wrong");
	}
}

module.exports.removeAllProductInOrder = async (req, res) => {
	try {
		const user = auth.decode(req.headers.authorization)
		let result = await User.findById({ _id: user.id })
		result.orders = [];
		result.save()
		return res.status(200).send(result);
	} catch (error){
		res.status(500).send("Something went wrong");
	}
}

//Create order - user checkout
module.exports.createOrder = async (req, res) => {
	try {
		const errors = validationResult(req)
  		if (!errors.isEmpty()) {
  			res.status(400).send({ errors: errors.array() });
  			return;
  		};

		let data = {
			userId: auth.decode(req.headers.authorization).id, 
			productId: req.body.productId,
			quantity: req.body.quantity
		};

		let user = await User.findById(data.userId);
		let orderedProduct = await Product.findOne({ _id: data.productId, isActive: true});
		if(orderedProduct) {
			let isExistingOrder = await user.orders.findIndex(x => {
				return x.productId == orderedProduct._id
			});
			if(isExistingOrder !== -1) {
				let order = user.orders[isExistingOrder]
				order.quantity++
				order.totalAmount += orderedProduct.price
				user.orders[isExistingOrder] = order
			} else {
				user.orders.push({
					productId: orderedProduct.id,
					quantity: data.quantity,
					totalAmount: parseFloat(orderedProduct.price * data.quantity)
				})
				orderedProduct.customers.push({
					userId: user.id
				})
			}
			await user.save();
			await orderedProduct.save();
			return res.status(200).send({
				msg: 'Order Complete',
				orders: user.orders
			});
		}

		return res.status(500).send('Product does not exist!');
	} catch (error) {
		res.status(500).send("Something went wrong");
	}
};

//Per user's order/s
module.exports.getUserOrder = async (req, res) => {
	try {
		let user = auth.decode(req.headers.authorization)
		let result = await User.findById(user.id).select('orders -_id');
		let productOrder = result.orders.map(async (order) => {
			return  {
				_id: order._id,
				product: await Product.findById(order.productId).select('-customers -__v'),
				quantity: order.quantity,
				totalAmount: order.totalAmount
			}
		})
		Promise.all(productOrder).then(data => res.status(200).send(data))
	} catch(error) {
		res.status(500).send("Something went wrong");
	}
	
};

//Orders of all users (Admin only)
module.exports.getAllOrder = async (req, res) => {
	try {
		if (!auth.decode(req.headers.authorization).isAdmin){
			return res.status(400).send('Unauthorized access.');
		};
		return User.find({}).select( 'orders' ).then(results => {
			return res.status(200).send(results.filter(result => result.orders.length))
		});
	} catch (error) {
		res.status(500).send("Something went wrong");
	} 
};
