const Cart = require('../models/cart');
const User = require('../models/user');
const Product = require('../models/product');
const Purchase = require('../models/purchase');
const auth = require('../auth');
const { validationResult } = require("express-validator")

//Create product (Admin only)
module.exports.addToCart = async (req, res) => {
	try {
        const productId = req.params.id
        let cart = new Cart(req.session.cart ? req.session.cart : {});
        let product = await Product.findById(productId)
        cart.add(product, productId);
        req.session.cart = cart;
		res.status(200).send(req.session.cart);
	} catch (error) {
		res.status(500).send("Something went wrong");
	}
};

module.exports.purchaseOrder = async (req, res) => {
	try {
        let userId = auth.decode(req.headers.authorization).id;
        let user = await User.findById(userId);
        if(user.orders.length) {
            let purchase = new Purchase({
                userId: userId,
                orders: user.orders
            })
            purchase = await purchase.save()
            user.orders = [];
            await user.save()
		    return res.status(200).send(purchase);
        } 
        throw 'No items in cart'
	} catch (error) {
		res.status(500).send("Something went wrong");
	}
};


