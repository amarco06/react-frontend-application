const express = require('express');
const router = express.Router();
const UserController = require('../controllers/userController');
const ValidationController = require('../controllers/validationController');
const auth = require('../auth');

//register a user
router.post(
	'/register',
	ValidationController.validate('register'),
	UserController.register
);

router.post(
	'/login',
	UserController.login
);

//routes for getting the info of the user
router.get(
	'/info', 
	auth.verify,
	UserController.getUserInfo
);

//create-order
router.post(
	'/create-order',
	auth.verify,
	ValidationController.validate('createOrder'),
	UserController.createOrder
);

//order list of a user
router.get(
	'/user-order', 
	auth.verify,
	UserController.getUserOrder
);

router.delete(
	'/user-order/remove/:id', 
	auth.verify,
	UserController.removeProductInOrder
);

router.delete(
	'/user-order/remove-all', 
	auth.verify,
	UserController.removeAllProductInOrder
);

//get all orders (Admin only)
router.get(
	'/all-orders',
	UserController.getAllOrder
);

router.get(
	'/purchase-history',
	auth.verify,
	UserController.getPurchaseHistory
);

module.exports = router