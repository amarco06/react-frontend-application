const express = require('express');
const router = express.Router();
const cartController = require('../controllers/cartController');
const ValidationController = require('../controllers/validationController');
const auth = require('../auth');

router.get(
	'/add/:id',
	cartController.addToCart
);

router.post(
	'/place-order',
	auth.verify,
	cartController.purchaseOrder
);




module.exports = router

