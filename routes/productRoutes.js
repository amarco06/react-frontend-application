const express = require('express');
const router = express.Router();
const ProductController = require('../controllers/productController');
const ValidationController = require('../controllers/validationController');
const auth = require('../auth');

//Create product (Admin only)

router.post(
	'/create-product',
	ValidationController.validate('saveProduct'),
	ProductController.createProduct
);

//Retrieve all active products
router.get(
	'/active-products', 
	ProductController.getActiveProducts
);

//Retrieve single product
router.get(
	'/:id',
	ProductController.getProductsById
);

//Update product
router.put(
	'/:id',
	ValidationController.validate('saveProduct'),
	ProductController.updateProduct
);

//Archive product
router.delete(
	'/:id',
	ProductController.archiveProduct
);


module.exports = router

