const mongoose = require('mongoose');
const userSchema = new mongoose.Schema({

	firstName: {
		type: String,
		required: [true, 'First name is required.']
	},

	lastName: {
		type: String,
		required: [true, 'Last name is required.']
	},

	email: {
		type: String,
		required: [true, 'Email is required.'],
		unique: true,
		dropDups: true,
	},

	password: {
		type: String,
		required: [true, 'Password is required.'],
	},

	contactNo: {
		type: String,
	},

	isAdmin: {
		type: Boolean,
		default: false
	},

	orders: [
	{
		productId: {
			type: String,
			required: [true, 'Order Id is required.']
		},
		purchasedOn: {
			type: Date,
			default: new Date()
		},
		quantity: {
			type: Number,
			required: [true, 'Quantity of product purchased is required.']
		},
		totalAmount: {
			type: Number,
			required: [true, 'Amount of purchased product is required.']
		}
	}
	]

})

module.exports = mongoose.model('User', userSchema)
