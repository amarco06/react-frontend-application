const mongoose = require('mongoose');


const productSchema = new mongoose.Schema({
	name: {
		type: String,
		required: [true, 'Product name is required.']
	},

	description: {
		type: String,
		required: [true, 'Description is required.']
	},

	image: {
		type: String,
	},

	price: {
		type: Number,
		required: [true, 'Price is required.']
	},
	
	quantity: {
		type: Number,
		required: [true, 'Quantity is required.']
	},

	isActive: {
		type: Boolean,
		default: true
	},

	createdOn: {
		type: Date,
		default: new Date()
	},

	customers: [
	{
		userId: {
			type: String,
			required: [true, 'User Id is required.']
		},
		purchasedOn: {
			type: Date,
			default: new Date()
		}
	}]

})

module.exports = mongoose.model('Product', productSchema)