const mongoose = require('mongoose');


const purchaseSchema = new mongoose.Schema({
    userId: {
        type: String,
        required: [true, 'User Id is required.']
    },


	createdOn: {
		type: Date,
		default: new Date()
	},

    orders: {
        type: Array,
        require: [true, 'orders is required.']
    }
	
})

module.exports = mongoose.model('Purchase', purchaseSchema)