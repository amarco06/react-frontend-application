const jwt = require('jsonwebtoken');
const secret = 'StopLookAndListen';

//3 main parts
//1. creation of the token -> analogy: pack the gift, and sign with the secret

module.exports.createAccessToken = (user) => {

	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}

	return jwt.sign(data, secret, {})
}

//2. verification of the token -> analogy: receive the gift and verify if the sender is legitimate and the gift was not tampered with.

module.exports.verify = (req, res, next) => {
	let token = req.headers.authorization

	if(typeof token !== 'undefined'){
		token = token.slice(7, token.length)
		return jwt.verify(token, secret, (err, data) => {
			return (err) ? res.send({ auth:'failed' }) : next()
		})
	} else {
		return res.send({ auth: 'failed' })
	}
}

//3. decoding of the token -> analogy: open the gift and get the content

	module.exports.decode = (token) => {
		if(typeof token !== 'undefined'){
			token = token.slice(7, token.length)

			return jwt.verify(token, secret, (err, data) => {
				return (err) ? null : jwt.decode(token, {complete: true }).payload
			})
		} else {
			return null
		}
	}