import React from 'react';
import { MDBCarousel, MDBCarouselCaption, MDBCarouselInner, MDBCarouselItem, MDBView, MDBMask, MDBContainer } from
"mdbreact";

export default function FeatureItems() {
    return (
        <MDBContainer>
            <MDBCarousel
                activeItem={1}
                length={3}
                showControls={true}
                showIndicators={true}
                className="z-depth-1"
            >
                <MDBCarouselInner>
                    <MDBCarouselItem itemId="1">
                        <MDBView>
                            <img
                                className="d-block w-100 img-fluid"
                                src="/assets/images/1.jpeg"
                                alt="First slide"
                                style={{maxHeight: '480px'}}
                            />
                            <MDBMask overlay="black-light" />
                        </MDBView>
                    </MDBCarouselItem>
                    <MDBCarouselItem itemId="2">
                        <MDBView>
                            <img
                                className="d-block w-100"
                                src="/assets/images/allmenu.jpeg"
                                alt="Second slide"
                                style={{maxHeight: '480px'}}
                            />
                            <MDBMask overlay="black-light" />
                        </MDBView>
                    </MDBCarouselItem>
                    <MDBCarouselItem itemId="3">
                        <MDBView>
                            <img
                                className="d-block w-100"
                                src="/assets/images/11.jpeg"
                                alt="Third slide"
                                style={{maxHeight: '480px'}}
                            />
                            <MDBMask overlay="black-slight" />
                        </MDBView>
                    </MDBCarouselItem>
                </MDBCarouselInner>
            </MDBCarousel>
        </MDBContainer>
    )
}