
import React, { useContext, useEffect, useState } from 'react'
import { MDBContainer, MDBBtn, MDBModal, MDBModalBody, MDBModalHeader, MDBModalFooter, MDBRow, MDBCol, MDBInput } from 'mdbreact';
import { Form, Image } from 'react-bootstrap';
import { Redirect, useHistory } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../../UserContext';

export default function AddProductModal(props) {
    const { user } = useContext(UserContext);
    const history = useHistory();
    const DEFAULT_IMAGE = 'https://dgpinnaclelos.com/wp-content/plugins/tutor/assets/images/placeholder.jpg';

    const [formType, setFormType] = useState('create');
    const [productId, setProductId] = useState(0);
    const [productName, setProductName] = useState('');
    const [description, setDescription] = useState('');
    const [quantity, setQuantity] = useState(0);
    const [price, setPrice] = useState(0);
    const [image, setImage] = useState('');
    const [imagePreview, setImagePreview] = useState(DEFAULT_IMAGE);

    React.useEffect(() => {
        if (props.product) {
            setFormType('update')
            setProductId(props.product._id)
            setProductName(props.product.name)
            setDescription(props.product.description)
            setQuantity(props.product.quantity)
            setPrice(props.product.price)
            setImagePreview(props.product.image || DEFAULT_IMAGE)
        } else {
            setFormType('create')
            setProductId('')
            setProductName('')
            setDescription('')
            setQuantity(0)
            setPrice(0)
            setImagePreview(DEFAULT_IMAGE)
        }
    }, [props.show]);


    function handleChange(event) {
        if (event.target.files[0]) {
            setImage(event.target.files[0]);
            const reader = new FileReader();
            reader.addEventListener("load", () => {
                setImagePreview(URL.createObjectURL(event.target.files[0]))
            });
            reader.readAsDataURL(event.target.files[0]);
        }
    }

    async function uploadProductImage() {
        let formData = new FormData()
        let imageLink = '';
        if (image) {
            formData.append('images', image)
            await fetch('http://localhost:4000/single', {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${user.accessToken}`
                },
                body: formData
            })
                .then(response => response.json())
                .then(data => {
                    imageLink = data.data
                })
            return imageLink
        }
        return ''
    }

    async function submit(e) {
        e.preventDefault();
        if (formType === 'create') {
            addProduct()
        } else if (formType === 'update') {
            updateProduct()
        }
    }

    async function updateProduct(e) {
        let imageLink = await uploadProductImage()
        fetch(`http://localhost:4000/products/${productId}`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${user.accessToken}`
            },
            body: JSON.stringify({
                name: productName,
                description: description,
                quantity: quantity,
                price: price,
                image: imageLink || imagePreview
            })
        })
            .then(response => response.json())
            .then(data => {
                Swal.fire({
                    icon: 'success',
                    text: 'Successfully update a product'
                })
                    .then(() => history.go(0))

            })
    }

    async function addProduct(e) {
        let imageLink = await uploadProductImage()
        fetch('http://localhost:4000/products/create-product', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${user.accessToken}`
            },
            body: JSON.stringify({
                name: productName,
                description: description,
                quantity: quantity,
                price: price,
                image: imageLink
            })
        })
            .then(response => response.json())
            .then(data => {
                Swal.fire({
                    icon: 'success',
                    text: 'Successfully add a product'
                })
                    .then(() => history.go(0))
            })
    }

    return (
        <MDBModal isOpen={props.show} toggle={props.onHide} size="lg">
            <MDBModalHeader className="text-capitalize" toggle={props.onHide}>{formType} Product</MDBModalHeader>
            <form onSubmit={(e) => submit(e)}>
                <MDBModalBody>
                    <MDBRow>
                        <MDBCol sm="12" md="6">
                            <Image style={{ maxHeight: '300px' }} className="mb-4" src={imagePreview} fluid />
                            <Form.Group className="mb-3">
                                <Form.Control type="file" placeholder="Upload product image" onChange={handleChange} />
                            </Form.Group>
                        </MDBCol>

                        <MDBCol sm="12" md="6">
                            <div className="grey-text">
                                <MDBInput
                                    label="Enter the name of the product"
                                    group
                                    type="text"
                                    validate
                                    error="wrong"
                                    success="right"
                                    value={productName}
                                    onChange={e => setProductName(e.target.value)}
                                    required
                                />
                                <MDBInput
                                    label="Describe your product"
                                    group
                                    type="text"
                                    validate
                                    error="wrong"
                                    success="right"
                                    value={description}
                                    onChange={e => setDescription(e.target.value)}
                                    required
                                />
                                <MDBInput
                                    label="Enter product quantity"
                                    group
                                    type="number"
                                    validate
                                    error="wrong"
                                    success="right"
                                    value={quantity}
                                    onChange={e => setQuantity(e.target.value)}
                                    required
                                />
                                <MDBInput
                                    label="Enter product price"
                                    group
                                    type="number"
                                    validate
                                    error="wrong"
                                    success="right"
                                    required
                                    value={price}
                                    onChange={e => setPrice(e.target.value)}
                                />
                            </div>
                        </MDBCol>

                    </MDBRow>
                </MDBModalBody>
                <MDBModalFooter>
                    <MDBBtn color="danger" onClick={props.onHide}>Close</MDBBtn>
                    <MDBBtn type="submit">Add Product</MDBBtn>
                </MDBModalFooter>
            </form>

        </MDBModal>
    )
}