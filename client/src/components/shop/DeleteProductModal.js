
import React, { useContext, useEffect, useState } from 'react'
import { MDBContainer, MDBBtn, MDBModal, MDBModalBody, MDBModalHeader, MDBModalFooter } from 'mdbreact';
import { Redirect, useHistory } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../../UserContext';

export default function DeleteProductModal(props) {
    const { user } = useContext(UserContext);
    const history = useHistory();

    function handleDelete(e) {
        e.preventDefault()
        fetch(`http://localhost:4000/products/${props.product}`, {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${user.accessToken}`
            }
        })
            .then(res => res.json())
            .then(data => {
                if (data) {
                    Swal.fire({
                        icon: 'success',
                        text: 'Successfully deleted course'
                    })
                        .then(() => history.go(0))
                } else {
                    Swal.fire({
                        icon: 'error',
                        text: 'Failed to delete a course'
                    });
                }
            })
    }
    return (

        <MDBModal isOpen={props.show} toggle={props.onHide} centered>
          <MDBModalHeader toggle={props.onHide}>Delete Product?</MDBModalHeader>
          <MDBModalBody>
            Are you sure you want to delete this product>
          </MDBModalBody>
          <MDBModalFooter>
            <MDBBtn color="secondary" onClick={props.onHide}>Close</MDBBtn>
            <MDBBtn color="danger" onClick={handleDelete}>Delete</MDBBtn>
          </MDBModalFooter>
        </MDBModal>
    )
}