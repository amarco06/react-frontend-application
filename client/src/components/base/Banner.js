
import React from 'react'
import { MDBJumbotron, MDBContainer, MDBBreadcrumb, MDBBreadcrumbItem } from "mdbreact";
import { useLocation } from 'react-router-dom';

export default function Banner(props) {
    return (
        <MDBJumbotron className="p-4">
            <MDBContainer>
                <h3 className="h3 text-capitalize">{props.currentPath}</h3>
                <MDBBreadcrumb className="bg-transparent">
                    <MDBBreadcrumbItem>
                        <a href="/">Home</a>
                    </MDBBreadcrumbItem>
                    <MDBBreadcrumbItem active className="text-capitalize">
                        {props.currentPath}
                    </MDBBreadcrumbItem>
                </MDBBreadcrumb>
            </MDBContainer>
        </MDBJumbotron>
    )
}