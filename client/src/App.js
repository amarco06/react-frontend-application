import React, { useState }  from 'react';
import './App.css';
import { BrowserRouter as Routers } from 'react-router-dom';
import { Route, Switch } from 'react-router-dom';
import { Container } from 'react-bootstrap';

import Navigation from './components/base/navbar/Navigation';
import Footer from './components/base/Footer';

import Login from './pages/Login'
import Register from './pages/Register'
import Cart from './pages/Cart'
import Home from './pages/Home'
import Shop from './pages/Shop'
import NotFound from './pages/NotFound'

import ViewProduct from './pages/product/ViewProduct';

import OrderHistory from './pages/user/OrderHistory';
import Profile from './pages/user/Profile';

import UserContext from './UserContext';

function App() {

	const [user, setUser] = useState({
        accessToken : localStorage.getItem('accessToken'),
		firstName   : localStorage.getItem('firstName'),
        email       : localStorage.getItem('email'),
        isAdmin     : localStorage.getItem('isAdmin') === 'true',
        isLogin     : localStorage.getItem('isLogin') === 'true'
    });

	const unsetUser = () =>{
        localStorage.clear();
        setUser({
            accessToken: null,
			firstName  : null,
            email      : null,
            isAdmin    : false,
            isLogin    : false
        })
    }

	return (
		<UserContext.Provider value={{ user, setUser, unsetUser }}>
			<Routers>
				<Navigation />
				<Container>
					<Switch>
						<Route exact path="/" component={Home} />
						<Route exact path="/shop" component={Shop} />
						<Route exact path="/login" component={Login} />
						<Route exact path="/register" component={Register} />

						<Route exact path="/cart" component={Cart} />
						<Route exact path="/products/:productId" component={ViewProduct} />
						<Route exact path="/users/info" component={Profile} />
						<Route exact path="/user/purchases" component={OrderHistory} />

						<Route component={NotFound}/>
					</Switch>
				</Container>
				<Footer />
			</Routers>
		</UserContext.Provider>
	);
};

export default App;
