import React, { useState, useEffect, useContext } from 'react';
import { Row, Col, Card, Image, Button } from 'react-bootstrap';
import { Redirect, useHistory } from 'react-router-dom';
import UserContext from '../../UserContext';
import Swal from 'sweetalert2';
import Banner from '../../components/base/Banner';
import '../../index.css'

function ViewProduct(props) {
    const history = useHistory();

    const { user } = useContext(UserContext);
    const productId = props.match.params.productId;
    const [product, setProduct] = useState([]);

    useEffect(() => {

        fetch(`http://localhost:4000/products/${productId}`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${user.accessToken}`
            },
        })
            .then(res => res.json())
            .then(data => {
                setProduct(data)
            })
    }, []);


    function addToCart(productId) {
        fetch(`http://localhost:4000/users/create-order`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${user.accessToken}`,
            },
            body: JSON.stringify({
                productId: productId,
                quantity: 1
            })
        })
            .then(res => res.json())
            .then(data => {
                Swal.fire({
                    icon: 'success',
                    text: 'Added to cart!'
                })
                    .then(() => history.go(0))
            })
    }

    return (
        <div className="my-5">
            <Banner currentPath="product" pathname="/products/view" />
            <Card className="p-5 my-5" >
                <Row>
                    <Col>
                        <Image src={product.image} fluid thumbnail />
                    </Col>
                    <Col>
                        <h3>{product.name}</h3>
                        <span className="my-5">{product.price} PHP</span>
                        <p className="mt-3">{product.description}</p>

                        <span>Quantity: {product.quantity}</span>
                        <div class="mt-5 d-flex">
                            <Button primary onClick={() => addToCart(product._id)}>Add to cart</Button>
                        </div>
                    </Col>
                </Row>
            </Card>
        </div>
    )
}

export default ViewProduct