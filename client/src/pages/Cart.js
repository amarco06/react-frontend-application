import React, { useEffect, useContext, useState } from 'react';
import { Card, Row, Col, Image, Button, Form } from 'react-bootstrap'
import { MDBRow, MDBCol, MDBCard, MDBCardBody, MDBCardText, MDBCardTitle, MDBBtn,  } from "mdbreact";
import * as AiIcons from 'react-icons/ai';
import '../components/cart/cart.css';
import { Link, useHistory } from 'react-router-dom';
import Banner from '../components/base/Banner'
import Swal from 'sweetalert2';


import UserContext from '../UserContext';
export default function Cart() {
    const { user } = useContext(UserContext);
    const history = useHistory();

    const [cartList, setCartList] = useState([]);
    const [overAllTotal, setOverAllTotal] = useState(0);

    function removeFromCart(productId) {
        Swal.fire({
            title: 'Are you sure?',
            text: "Item will be remove form cart!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Remove from cart'
        }).then((result) => {
            if (result.isConfirmed) {
                fetch(`http://localhost:4000/users/user-order/remove/${productId}`, {
                    method: 'DELETE',
                    headers: {
                        Authorization: `Bearer ${user.accessToken}`,
                    },
                })
                    .then(res => res.json())
                    .then(data => {
                        Swal.fire({
                            icon: 'success',
                            text: 'Successfully remove product from cart'
                        })
                            .then(() => history.go(0))
                    })
            }
        })
    }

    function placeOrder() {
        Swal.fire({
            title: 'Are you sure?',
            text: "Items in cart will be place in orders",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Place Order'
        }).then((result) => {
            if (result.isConfirmed) {
                fetch(`http://localhost:4000/cart/place-order`, {
                    method: 'POST',
                    headers: {
                        Authorization: `Bearer ${user.accessToken}`,
                    },
                })
                    .then(res => res.json())
                    .then(data => {
                        Swal.fire({
                            icon: 'success',
                            text: 'Successfully purchased products from cart'
                        })
                            .then(() => history.go(0))
                    })
            }
        })
    }

    function clearCart(productId) {
        Swal.fire({
            title: 'Are you sure?',
            text: "All items in the cart will be remove",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Remove from cart'
        }).then((result) => {
            if (result.isConfirmed) {
                fetch(`http://localhost:4000/users/user-order/remove-all`, {
                    method: 'DELETE',
                    headers: {
                        Authorization: `Bearer ${user.accessToken}`,
                    },
                })
                    .then(res => res.json())
                    .then(data => {
                        Swal.fire({
                            icon: 'success',
                            text: 'Successfully remove products from cart'
                        })
                            .then(() => history.go(0))
                    })
            }
        })
    }

    useEffect(() => {
        fetch('http://localhost:4000/users/user-order', {
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${user.accessToken}`,
            },
        })
            .then(res => res.json())
            .then(data => {
                let cartTotal = 0
                setCartList(data.map(item => {
                    cartTotal += item.totalAmount
                    return (
                        <MDBRow key={item._id} className="mt-3">
                            <MDBCol sm={6}>
                                <div class="d-flex ms-4">
                                    <img className="cart-thumb img-fluid" src={item.product.image} rounded fluid thumbnail />
                                    <div className="ms-3">
                                        <h4>{item.product.name}</h4>
                                        <p>{item.product.description}</p>
                                    </div>
                                </div>
                            </MDBCol>
                            <MDBCol sm={2} className="d-flex align-items-center">{item.product.price} PHP</MDBCol>
                            <MDBCol sm={2} className="d-flex align-items-center">{item.quantity}</MDBCol>
                            <MDBCol sm={1} className="d-flex align-items-center">{item.totalAmount}</MDBCol>
                            <MDBCol sm={1} className="d-flex align-items-center">
                                <AiIcons.AiFillCloseCircle onClick={() => removeFromCart(item.product._id)} />
                            </MDBCol>
                        </MDBRow>
                    )
                }))
                setOverAllTotal(cartTotal)
            })
    }, [])

    return (
        <>
            <div className="my-5">
                <Banner currentPath="cart" pathname="/cart" />
                <MDBCard className="mt-5">
                    <MDBCardBody>
                        <MDBRow>
                            <MDBCol sm={6}>Product</MDBCol>
                            <MDBCol sm={2}>Price</MDBCol>
                            <MDBCol sm={2}>Quantity</MDBCol>
                            <MDBCol sm={1}>Total</MDBCol>
                            <MDBCol sm={1}></MDBCol>
                        </MDBRow>
                        {cartList}
                    </MDBCardBody>
                </MDBCard>

                <MDBRow>
                    <div class="mt-3 d-flex justify-content-end">
                        <MDBBtn color="danger" onClick={() => clearCart() } >Clear Cart</MDBBtn>
                    </div>
                </MDBRow>

                <MDBRow className="my-5">
                    <MDBCol sm={12} md={6}>
                        <MDBCard className="p-3">
                            <MDBCardBody>
                                <MDBCardTitle>Billing Details</MDBCardTitle>
                                <MDBCardText>
                                    <form>
                                        <MDBRow className="mb-3">
                                            <MDBCol>
                                                <label htmlFor="defaultFormLoginEmailEx" className="grey-text">
                                                    First Name
                                                </label>
                                                <input type="text" id="defaultFormLoginEmailEx" className="form-control" placeholder="Enter first name" />
                                            </MDBCol>

                                            <MDBCol>
                                                <label htmlFor="defaultFormLoginEmailEx" className="grey-text">
                                                    Last Name
                                                </label>
                                                <input type="text" id="defaultFormLoginEmailEx" className="form-control" placeholder="Enter last name" />
                                            </MDBCol>

                                        </MDBRow>

                                        <MDBRow className="mb-3">
                                            <MDBCol>
                                                <label htmlFor="defaultFormLoginEmailEx" className="grey-text">
                                                    Email
                                                </label>
                                                <input type="email" id="defaultFormLoginEmailEx" className="form-control" placeholder="Enter email" />
                                            </MDBCol>
                                        </MDBRow>

                                        <MDBRow className="mb-3">
                                            <MDBCol>
                                                <label htmlFor="defaultFormLoginEmailEx" className="grey-text">
                                                    Address
                                                </label>
                                                <input type="text" id="defaultFormLoginEmailEx" className="form-control" placeholder="1234 Main St" />
                                            </MDBCol>
                                        </MDBRow>

                                        <MDBRow className="mb-4">
                                            <MDBCol>
                                                <label htmlFor="defaultFormLoginEmailEx" className="grey-text">
                                                    City
                                                </label>
                                                <input type="text" id="defaultFormLoginEmailEx" className="form-control" placeholder="City" />
                                            </MDBCol>

                                            <MDBCol>
                                                <label htmlFor="defaultFormLoginEmailEx" className="grey-text">
                                                    State
                                                </label>
                                                <input type="text" id="defaultFormLoginEmailEx" className="form-control" placeholder="state" />
                                            </MDBCol>

                                            <MDBCol>
                                                <label htmlFor="defaultFormLoginEmailEx" className="grey-text">
                                                    Zip
                                                </label>
                                                <input type="text" id="defaultFormLoginEmailEx" className="form-control" placeholder="Zip code" />
                                            </MDBCol>
                                        </MDBRow>

                                        <MDBRow className="mt-3">
                                            <MDBCol>
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input" id="defaultChecked2" />
                                                    <label class="custom-control-label" for="defaultChecked2">By checking this box you agree to terms and agreement</label>
                                                </div>
                                            </MDBCol>
                                        </MDBRow>
                                    </form>
                                </MDBCardText>
                            </MDBCardBody>
                        </MDBCard>
                    </MDBCol>
                    <MDBCol sm={12} md={6}>
                        <MDBCard>
                            <MDBCardBody>
                                <MDBCardTitle>Cart Total</MDBCardTitle>
                                <MDBCardText className="p-3">
                                    <div className="d-flex justify-content-between">
                                        <span>Subtotal</span>
                                        <span>{overAllTotal} PHP</span>
                                    </div>

                                    <div className="d-flex justify-content-between">
                                        <span>Total</span>
                                        <span>{overAllTotal} PHP</span>
                                    </div>
                                </MDBCardText>
                                <div className="d-flex justify-content-end mt-4">
                                    <MDBBtn onClick={() => history.push(`/shop`)} color="secondary">Continue Shopping</MDBBtn>
                                    <MDBBtn color="primary" onClick={() => placeOrder()}>Place Order</MDBBtn>
                                </div>
                            </MDBCardBody>
                        </MDBCard>
                    </MDBCol>
                </MDBRow>
            </div>
        </>
    )
}