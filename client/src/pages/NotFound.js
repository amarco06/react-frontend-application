import React from 'react';
import { Link } from 'react-router-dom'
import { Button } from 'react-bootstrap';
import { MDBBtn, MDBCard, MDBCardBody, MDBCardImage, MDBCardTitle, MDBCardText, MDBCol } from 'mdbreact';

export default function NotFound() {
	return (
		<MDBCard className="my-5" style={{padding: '80px'}}>
			<MDBCardBody>
				<MDBCardTitle>Page Not Found/Error 404</MDBCardTitle>
				<MDBCardText>
					We couldn't find the page that you are looking for.
				</MDBCardText>
				<MDBBtn href="/">
					Go back to the homepage.
				</MDBBtn>
			</MDBCardBody>
		</MDBCard>

	)
}