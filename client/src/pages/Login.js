import React, { useState, useEffect, useContext } from 'react';
import { MDBContainer, MDBRow, MDBCol, MDBInput, MDBBtn, MDBCard, MDBCardBody } from 'mdbreact';

import Swal from 'sweetalert2';
import { Redirect, useHistory } from 'react-router-dom';
import validator from 'validator'
import * as FaIcons from "react-icons/fa";
import { RiLockFill } from "react-icons/ri";
import UserContext from '../UserContext';
import '../index.css';

const Login = () => {

    const { user, setUser } = useContext(UserContext);

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [loginButton, setLoginButton] = useState(false);

    const history = useHistory();

    useEffect(() => {

        if ((email && password) && (validator.isEmail(email))) {
            setLoginButton(true);
        } else {
            setLoginButton(false);
        }
    }, [email, password]);

    function loginUser(e) {
        e.preventDefault();

        fetch('http://localhost:4000/users/login', {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
            .then(res => res.json())
            .then(data => {
                const accessToken = data.accessToken
                if (data.accessToken !== undefined) {
                    localStorage.setItem('accessToken', data.accessToken);

                    Swal.fire({
                        title: 'Great!',
                        icon: 'success',
                        text: 'You are now logged in. Have a nice day!'
                    });

                    fetch('http://localhost:4000/users/info', {
                        headers: {
                            Authorization: `Bearer ${data.accessToken}`
                        }
                    })
                        .then(res => res.json())
                        .then(data => {
                            localStorage.setItem('firstName', data.firstName)
                            localStorage.setItem('email', data.email);
                            localStorage.setItem('isAdmin', data.isAdmin);
                            localStorage.setItem('isLogin', true);

                            setUser({
                                email: data.email,
                                firstName: data.firstName,
                                isAdmin: data.isAdmin,
                                isLogin: true,
                                accessToken: accessToken
                            })
                            history.push('/shop')

                        })
                        


                } else {
                    Swal.fire({
                        title: 'Oooops!',
                        icon: 'error',
                        text: 'Something Went Wrong. Check your Credentials'
                    })
                }
                setEmail('');
                setPassword('');
            })
            .catch(err => {
                Swal.fire({
                    icon: 'error',
                    text: 'Invalid Email or password'
                })
            })
    }

    if (user.accessToken !== null) {
        return <Redirect to="/" />
    }

    return (
        <MDBContainer className="my-5">
            <MDBCard>
                <MDBCardBody>
                    <MDBRow>
                        <MDBCol md="6">
                            <img src="https://www.getillustrations.com/packs/flat-vector-illustrations-for-websites/scenes/_1x/accounts%20_%20user,%20profile,%20login,%20password,%20username_md.png" className="img-fluid" alt="" />
                        </MDBCol>
                        <MDBCol md="6" className="align-self-center p-5">
                            <form onSubmit={(e) => loginUser(e)}>
                                <p className="h5 text-center mb-4">Log in</p>
                                <div className="grey-text">
                                    <MDBInput
                                        label="Type your email"
                                        icon="envelope"
                                        group
                                        type="email"
                                        validate
                                        error="wrong"
                                        onChange={e => setEmail(e.target.value)}
                                        success="right" />

                                    <MDBInput
                                        label="Type your password"
                                        icon="lock"
                                        group
                                        type="password"
                                        onChange={e => setPassword(e.target.value)}
                                        validate />
                                </div>
                                <div className="text-center">
                                    <p className="font-small grey-text d-flex justify-content-end">
                                        Not a member?
                                        <a href="/register" className="blue-text ml-1">

                                            Sign Up
                                        </a>
                                    </p>
                                    <MDBBtn type="submit" disabled={!loginButton}>Login</MDBBtn>
                                </div>
                            </form>
                        </MDBCol>
                    </MDBRow>
                </MDBCardBody>
            </MDBCard>
        </MDBContainer>
    );
};

export default Login;