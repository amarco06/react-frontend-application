import React, { useState, useEffect, useContext } from 'react';
import UserContext from '../../UserContext';
import { MDBBtn, MDBCard, MDBCardBody, MDBCardImage, MDBCardTitle, MDBCardText, MDBView, MDBCol, MDBIcon } from 'mdbreact';
import '../../index.css'
function ViewUserInfo() {

    const { user } = useContext(UserContext);

    const [profile, setProfile] = useState({});

    useEffect(() => {

        fetch(`http://localhost:4000/users/info`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${user.accessToken}`
            },
        })
            .then(res => res.json())
            .then(data => {
                setProfile(data)
            })
    }, []);

    return (
        <>
            <MDBCard wide cascade className="mx-auto my-5 w-50">
                <MDBView cascade>
                    <MDBCardImage
                        hover
                        overlay='white-slight'
                        className='card-img-top'
                        src='https://mdbcdn.b-cdn.net/img/Photos/Horizontal/People/6-col/img%20%283%29.jpg'
                        alt='Card cap'
                    />
                </MDBView>

                <MDBCardBody cascade className='text-center'>
                    <MDBCardTitle className='card-title text-capitalize'>
                        <strong>{profile.firstName} {profile.lastName}</strong>
                    </MDBCardTitle>

                    <p className='font-weight-bold blue-text'>{profile.email}</p>

                    <MDBCardText>
                        This is my biography
                    </MDBCardText>

                    <MDBCol md='12' className='d-flex justify-content-center'>
                        <a href='!#' className='px-2 fa-lg li-ic'>
                            <MDBIcon fab icon='linkedin-in'></MDBIcon>
                        </a>

                        <a href='!#' className='px-2 fa-lg tw-ic'>
                            <MDBIcon fab icon='twitter'></MDBIcon>
                        </a>

                        <a href='!#' className='px-2 fa-lg fb-ic'>
                            <MDBIcon fab icon='facebook-f'></MDBIcon>
                        </a>
                    </MDBCol>
                </MDBCardBody>
            </MDBCard>

        </>
    )
}

export default ViewUserInfo