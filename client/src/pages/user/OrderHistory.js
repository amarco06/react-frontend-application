import React, { useState, useEffect, useContext } from 'react';
import UserContext from '../../UserContext';
import { MDBRow, MDBCol, MDBCard, MDBCardBody, MDBCardImage, MDBCardTitle, MDBCardText, MDBView, MDBIcon } from 'mdbreact';
import Banner from '../../components/base/Banner';
import { Redirect, useHistory } from 'react-router-dom';
export default function OrderHistory() {

    const { user } = useContext(UserContext);
    const [cartList, setCartList] = useState([]);
    
    

    useEffect(() => {
        if(user.accessToken === null) return
        fetch(`http://localhost:4000/users/purchase-history`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${user.accessToken}`
            },
        })
            .then(res => res.json())
            .then(data => {
                setCartList(data.map(item => {
                    return (
                        <MDBCard className="mt-5">
                            <MDBCardBody>
                                {
                                    item.orders.map(product => {
                                        return (
                                            <>
                                                <MDBRow className="mt-3">
                                                    <MDBCol sm={6}>
                                                        <div class="d-flex ms-4">
                                                            <img className="cart-thumb img-fluid" src={product.product.image} rounded fluid thumbnail />
                                                            <div className="ms-3">
                                                                <h4>{product.product.name}</h4>
                                                                <p>{product.product.description}</p>
                                                            </div>
                                                        </div>
                                                    </MDBCol>
                                                    <MDBCol sm={2} className="d-flex align-items-center">{product.product.price} PHP</MDBCol>
                                                    <MDBCol sm={2} className="d-flex align-items-center">{product.quantity}</MDBCol>
                                                    <MDBCol sm={1} className="d-flex align-items-center">{product.totalAmount}</MDBCol>
                                                </MDBRow>
                                            </>
                                        )
                                    })
                                }

                                <MDBRow>
                                    <MDBCol className="d-flex flex-row-reverse">
                                        <h5>
                                            <strong>Total Amount: {item.subTotal}</strong>
                                        </h5>
                                    </MDBCol>
                                </MDBRow>
                            </MDBCardBody>


                        </MDBCard>
                    )


                }))
            });
    }, [])

    if (user.accessToken === null) {
        return <Redirect to="/" />
    }

    

    return (
        <div className="my-5">
            <Banner currentPath="Order History" pathname="/user/order-history" />
            <MDBCard className="mt-5">
                <MDBCardBody>
                    <MDBRow>
                        <MDBCol sm={6}>Product</MDBCol>
                        <MDBCol sm={2}>Price</MDBCol>
                        <MDBCol sm={2}>Quantity</MDBCol>
                        <MDBCol sm={1}>Total</MDBCol>
                    </MDBRow>
                    {cartList}
                </MDBCardBody>
            </MDBCard>
        </div >
    )
}
